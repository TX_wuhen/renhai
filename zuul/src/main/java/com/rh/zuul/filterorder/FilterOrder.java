package com.rh.zuul.filterorder;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 57556
 */
public class FilterOrder {
    /**
     * 登陆过滤器
     */
    public static final int FILTER_LOGIN = 1;
    /**
     * 设置过滤器拦截基本
     */
    public static final int FILTER_HEADER = 2;

    public static final int FILTER_RESPONSER = 1;

    /**
     * 返回数据加密
     */
    public static final int FILTER_BACKENCRYPT = 99;
    /**
     * 忽略地址
     */
    public static final Map<String, String> OVERLOOK_URL;
    /**
     * 登录忽略地址
     */
    public static final Map<String, String> LOGINFILTER_URL = new HashMap() {
        {
            put("/renHai/user/api/users/user/login.action", "/renHai/user/api/users/user/login.action");//登陆
        }

    };

    static {
        OVERLOOK_URL = new HashMap() {
            {
                //http://127.0.0.1:8910/swagger-ui.html
                put("/renHai/user//v2/api-docs", "/renHai/user//v2/api-docs");//文档
                put("/renHai/file//v2/api-docs", "/renHai/file//v2/api-docs");//文档
                put("/renHai/redis//v2/api-docs", "/renHai/redis//v2/api-docs");//文档
                put("/renHai/log//v2/api-docs", "/renHai/log//v2/api-docs");//文档
                put("/renHai/file/api/file/file/chunkUpload", "/renHai/user//v2/api-docs");//上传文件
            }
        };
    }
}