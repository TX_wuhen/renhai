package com.rh.zuul.filtepost;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.rh.entity.log.RenHaiLog;
import com.rh.zuul.filterorder.FilterOrder;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;
import renhai.util.RestTemplateUtil;
import renhai.util.log.LogUrl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Date;

import static com.netflix.zuul.context.RequestContext.getCurrentContext;
import static org.springframework.util.ReflectionUtils.rethrowRuntimeException;

/**
 * 将返回数据加密
 */
@Component
public class BackEncrypt extends ZuulFilter {
    @Autowired
    private RestTemplateUtil restTemplateUtil;
    @Override
    public String filterType() {
        return FilterConstants.POST_TYPE;
    }

    @Override
    public int filterOrder() {
        return FilterOrder.FILTER_BACKENCRYPT;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RenHaiLog renHaiLog = new RenHaiLog();
        RequestContext context = getCurrentContext();
        HttpServletRequest request = context.getRequest();
        String url = request.getRequestURI();
        HttpServletResponse response = context.getResponse();
        String errorMsg = response.getHeader("errorMsg");
        if (StringUtils.isNotBlank(errorMsg)) {
            renHaiLog.setLogResponseData(errorMsg+"|");
            //返回数据加密
            //errorMsg = AesEncryptUtil.encrypt(errorMsg);
            response.setHeader("errorMsg", errorMsg);
        }
        if (FilterOrder.OVERLOOK_URL.get(url) == null) {
            try {
                InputStream stream = context.getResponseDataStream();
                String body = StreamUtils.copyToString(stream, Charset.forName("UTF-8"));
                System.out.println(url+"    返回数据：" + body);
                if(renHaiLog.getLogResponseData()!=null){
                    renHaiLog.setLogResponseData(renHaiLog.getLogResponseData()+body);
                }else{
                    renHaiLog.setLogResponseData(body);
                }
                if (StringUtils.isNotBlank(body)) {
                    //返回数据加密
                    //body = AesEncryptUtil.encrypt(body);
                }
                context.setResponseBody(body);
            } catch (Exception e) {
                rethrowRuntimeException(e);
            }
        }
        String logId = context.getZuulRequestHeaders().get("log_id");
        if(StringUtils.isNotBlank(logId)){
            renHaiLog.setLogId(Long.valueOf(logId));
            renHaiLog.setLogResponseTime(new Date());
            restTemplateUtil.postForObject2(LogUrl.SAVE_UPDATE, renHaiLog);
        }

        return null;
    }
}
