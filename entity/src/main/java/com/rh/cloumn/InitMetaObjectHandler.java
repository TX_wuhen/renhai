package com.rh.cloumn;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import renhai.util.DataUtil;
import renhai.util.RestTemplateUtil;
import renhai.util.id.IdUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 57556 字段默认值
 */
public class InitMetaObjectHandler implements MetaObjectHandler {

    private RestTemplateUtil restTemplateUtil;
    /**
     * 填充字段 添加
     */
    private final static Map<String, Object> INSERT_MAP = new HashMap<>();
    /**
     * 填充字段修改
     */
    private final static Map<String, Object> UPDATE_MAP = new HashMap<>();

    static {
        INSERT_MAP.put("createTime", DataUtil.getDataTime());
      /*  INSERT_MAP.put("createName","");
        INSERT_MAP.put("createId", "");
        INSERT_MAP.put("updateId", "");
        INSERT_MAP.put("updateName", "");*/
        INSERT_MAP.put("updateTime", DataUtil.getDataTime());
        INSERT_MAP.put("id", IdUtil.nextId());
        INSERT_MAP.put("status", 0);
        INSERT_MAP.put("del", 0);
    }

    static {
        UPDATE_MAP.put("updateTime", DataUtil.getDataTime());
    }

    public InitMetaObjectHandler(RestTemplateUtil restTemplateUtil) {

        this.restTemplateUtil = restTemplateUtil;
    }
    public InitMetaObjectHandler() {

    }

    @Override
    public void insertFill(MetaObject metaObject) {
        INSERT_MAP.forEach((k, v) -> {
            Object column = getFieldValByName(k, metaObject);
            if (column == null) {
                if (k.contains("Time")) {
                    setFieldValByName(k, new Date(), metaObject);
                }/*else if(k.equals("createId")){
                    RenHaiUser user = restTemplateUtil.getUser(RenHaiUser.class);
                    setFieldValByName(k,user.getUserId(), metaObject);
                }if(k.equals("createName")){
                    RenHaiUser user = restTemplateUtil.getUser(RenHaiUser.class);
                    setFieldValByName(k,user.getUserId(), metaObject);
                }if(k.equals("updateId")){
                    RenHaiUser user = restTemplateUtil.getUser(RenHaiUser.class);
                    setFieldValByName(k,user.getUserId(), metaObject);
                } else if(k.equals("createId")){
                    RenHaiUser user = restTemplateUtil.getUser(RenHaiUser.class);
                    setFieldValByName(k,user.getUserId(), metaObject);
                }*/ else {
                    setFieldValByName(k, v, metaObject);
                }

            }
        });
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        System.out.println(UPDATE_MAP);
        UPDATE_MAP.forEach((k, v) -> {
            Object column = getFieldValByName(k, metaObject);
            if (column == null) {
                if (k.contains("Time")) {
                    setFieldValByName(k, new Date(), metaObject);
                } else {
                    setFieldValByName(k, v, metaObject);
                }
            }
        });
    }
}
