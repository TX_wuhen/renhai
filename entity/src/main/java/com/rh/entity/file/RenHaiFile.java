package com.rh.entity.file;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @author wangdao
 */
@Getter
@Setter
@ApiModel("文件")
public class RenHaiFile {
    @ApiModelProperty(name = "fileId", value = "文件id")
    @TableId
    private String fileId;
    @ApiModelProperty(name = "url", value = "文件地址")
    private String url;
    @ApiModelProperty(name = "md5", value = "md5效验码")
    private String md5;
    /**
     * 是否上传完成 0 未完成 1 已完成
     */
    @ApiModelProperty(name = "complete", value = "是否上传完成 0 未完成 1 已完成 ")
    private Integer complete;
    @ApiModelProperty(name = "chunkSize", value = "分块大小单位b")
    private Integer chunkSize;
    @ApiModelProperty(name = "totalChunks", value = "文件被分成的块数")
    private Integer totalChunks;
    @ApiModelProperty(name = "totalSize", value = "文件总大小")
    private Long totalSize;

    @ApiModelProperty(name = "fileDetailsList", value = "上传成功分片信息")
    @TableField(exist = false)
    private List<RenHaiFileDetails> fileDetailsList;
    @ApiModelProperty(name = "fileDetailsChunkNumbers", value = "上传成功分片")
    @TableField(exist = false)
    private Long[] fileDetailsChunkNumbers;
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(name = "status", value = "状态")
    private Integer status;
    /**
     * 页面写入数据库时格式化
     */
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:sss")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:sss")
    private Date createTime;
    /**
     * 页面写入数据库时格式化
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:sss")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:sss")
    private Date updateTime;

    @ApiModelProperty(name = "path", value = "文件路径")
    private String path;

    public Long[] getFileDetailsChunkNumbers() {
        if (fileDetailsList != null && !fileDetailsList.isEmpty()) {
            Long[] temp = new Long[fileDetailsList.size()];
            for (int i = 0; i < fileDetailsList.size(); i++) {
                temp[i] = fileDetailsList.get(i).getChunkNumber();
            }
            return temp;
        } else {
            return this.fileDetailsChunkNumbers;
        }
    }


}
