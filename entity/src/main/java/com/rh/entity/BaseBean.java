package com.rh.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import renhai.util.json.LongJsonDeserializer;
import renhai.util.json.LongJsonSerializer;

import java.io.Serializable;
import java.util.Date;
/**
 * @author 57556
 */
@Getter
@Setter
public class BaseBean implements Serializable {

    @ApiModelProperty(value = "主键id")
    @TableId(type = IdType.ID_WORKER )
    @JsonDeserialize(using = LongJsonDeserializer.class)
    @JsonSerialize(using = LongJsonSerializer.class)
    private Long id;

    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:sss")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:sss")
    private Date createTime;

    @ApiModelProperty(value = "创建人id")
    @TableField("create_id")
    private String createId;

    @ApiModelProperty(value = "创建人姓名")
    @TableField("create_name")
    private String createName;

    @ApiModelProperty(value = "更新人")
    @TableField("update_id")
    private String updateId;

    @ApiModelProperty(value = "更新人姓名")
    @TableField("update_name")
    private String updateName;

    @ApiModelProperty(value = "更新时间")
    @TableField("update_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private Integer del;

}
