package com.rh.entity.user;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
@Getter
@Setter
public class RenHaiUserRole implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId
    private String userRoleId;
    private String renHaiUser;
    private String renHaiRole;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private Integer status;

    public RenHaiUserRole(String renHaiUser, String renHaiRole) {
        this.renHaiUser = renHaiUser;
        this.renHaiRole = renHaiRole;
    }

    public RenHaiUserRole() {
    }

    @Override
    public String toString() {
        return "RenHaiUserRole{" +
                ", userRoleId=" + userRoleId +
                ", renHaiUser=" + renHaiUser +
                ", renHaiRole=" + renHaiRole +
                ", createTime=" + createTime +
                "}";
    }
}
