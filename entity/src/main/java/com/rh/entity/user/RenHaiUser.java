package com.rh.entity.user;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;
import renhai.util.encrypt.PBEUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author 57556
 */
@Getter
@Setter
@ApiModel("用户")
public class RenHaiUser implements Serializable {

    @ApiModelProperty(name = "userId", value = "用户id")
    @TableId
    private String userId;
    @ApiModelProperty(name = "userName", value = "用户名")
    private String userName;
    @ApiModelProperty(name = "name", value = "姓名")
    private String name;

    @ApiModelProperty(name = "password", value = "密码")
    private String password;
    @JsonIgnore
    @ApiModelProperty(name = "salt", value = "盐")
    private String salt;
    @ApiModelProperty(name = "PasswordDecrypt", value = "解密后的密码")
    @TableField(exist = false)
    private String passwordDecrypt;
    @JsonIgnore
    @ApiModelProperty(name = "PasswordEncrypt", value = "加密后的密码")
    @TableField(exist = false)
    private String passwordEncrypt;

    /*    public String getPasswordDecrypt(){
            //密码，用户名，用户id
            if(StringUtils.isNotBlank(this.password)&& StringUtils.isNotBlank(this.salt) && StringUtils.isNotBlank(this.userId)){
                System.out.println(this.salt);
                System.out.println("asdasd"+PBEUtil.encrypt(this.password.getBytes(), this.userId, PBEUtil.varIntToByteArray(Long.parseLong(this.salt))));
                return PBEUtil.encrypt(this.password.getBytes(), this.userId, PBEUtil.varIntToByteArray(Long.parseLong(this.salt)));
            }
            return  "";
        }*/
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private Integer status;
    /**
     * 页面写入数据库时格式化
     */
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:sss")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:sss")
    private Date createTime;
    /**
     * 页面写入数据库时格式化
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:sss")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:sss")
    private Date updateTime;

    @ApiModelProperty(name = "roleIds", value = "角色id")
    @TableField(exist = false)
    private String[] roleIds;
    @ApiModelProperty(name = "roleIds", value = "角色id")
    @TableField(exist = false)
    private List<RenHaiUserRole> renHaiRoleList = new ArrayList<>();

    public String getPassword() {
        return "";
    }

    public String getPasswordEncrypt() {
        //密码，用户名，用户id
        if (StringUtils.isNotBlank(this.password) && StringUtils.isNotBlank(this.userId) && !StringUtils.isNotBlank(this.salt)) {
            byte[] salt = PBEUtil.initSalt();
            this.salt = Long.toString(PBEUtil.byteArrayToLong(salt));
            return PBEUtil.encrypt(this.password.getBytes(), this.userId, salt);
        }
        return "";
    }

    public String[] getRoleIds() {
        if (ArrayUtils.isNotEmpty(roleIds)) {
            return roleIds;
        } else {
            if (renHaiRoleList != null && !renHaiRoleList.isEmpty()) {
                String[] roleId = new String[renHaiRoleList.size()];
                for (int i = 0; i < renHaiRoleList.size(); i++) {
                    roleId[i] = renHaiRoleList.get(i).getRenHaiRole();
                }
                return roleId;
            }
        }
        return roleIds;
    }

}
