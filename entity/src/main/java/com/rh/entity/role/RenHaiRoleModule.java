package com.rh.entity.role;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
@Getter
@Setter
public class RenHaiRoleModule implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId
    private String roleModuleId;
    private String renHaiRole;
    private String renHaiModule;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private Integer status;

    public RenHaiRoleModule(String renHaiRole, String renHaiModule) {
        this.renHaiRole = renHaiRole;
        this.renHaiModule = renHaiModule;
    }

    @Override
    public String toString() {
        return "RenHaiRoleModule{" +
                ", roleModuleId=" + roleModuleId +
                ", renhaiRole=" + renHaiRole +
                ", renhaiModule=" + renHaiModule +
                ", createTime=" + createTime +
                "}";
    }
}
