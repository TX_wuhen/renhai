package com.rh.entity.log;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangdao
 * @since 2020-04-21
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="RenHaiLog对象", description="")
public class RenHaiLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "log_id", type = IdType.ID_WORKER )
    private Long logId;

    @ApiModelProperty(value = "操作人")
    @TableField("log_operation_user_name")
    private String logOperationUserName;

    @ApiModelProperty(value = "操作人id")
    @TableField("log_operation_user_id")
    private String logOperationUserId;

    @ApiModelProperty(value = "操作时间")
    @TableField(value = "log_operation_time",fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:sss")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:sss")
    private Date logOperationTime;

    @ApiModelProperty(value = "请求方法")
    @TableField("log_request_method")
    private String logRequestMethod;

    @ApiModelProperty(value = "请求地址")
    @TableField("log_request_url")
    private String logRequestUrl;

    @ApiModelProperty(value = "请求数据")
    @TableField("log_request_data")
    private String logRequestData;

    @ApiModelProperty(value = "请求ip")
    @TableField("log_request_ip")
    private String logRequestIp;

    @ApiModelProperty(value = "返回数据")
    @TableField("log_response_data")
    private String logResponseData;

    @ApiModelProperty(value = "返回时间")
    @TableField(value = "log_response_time",fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:sss")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:sss")
    private Date logResponseTime;


}
