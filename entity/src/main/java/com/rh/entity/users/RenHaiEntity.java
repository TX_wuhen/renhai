package com.rh.entity.users;

import com.baomidou.mybatisplus.annotation.TableField;
import com.rh.entity.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 *  
 * </p>
 *
 * @author wangdao
 * @since 2020-11-21
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="RenHaiEntity对象", description=" ")
public class RenHaiEntity extends BaseBean {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "实体名tableName")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "实体title")
    @TableField("title")
    private String title;


}
