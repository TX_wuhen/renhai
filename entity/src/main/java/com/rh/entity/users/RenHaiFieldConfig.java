package com.rh.entity.users;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rh.entity.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import renhai.util.json.LongJsonDeserializer;
import renhai.util.json.LongJsonSerializer;

/**
 * <p>
 *  
 * </p>
 *
 * @author wangdao
 * @since 2020-11-21
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="RenHaiFieldConfig对象", description=" ")
public class RenHaiFieldConfig extends BaseBean {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "属性名称")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "属性值")
    @TableField("value")
    private String value;

    @ApiModelProperty(value = "字段field")
    @TableField("field_id")
    @JsonDeserialize(using = LongJsonDeserializer.class)
    @JsonSerialize(using = LongJsonSerializer.class)
    private Long fieldId;

    @ApiModelProperty(value = "配置id")
    @TableField("parent_id")
    @JsonDeserialize(using = LongJsonDeserializer.class)
    @JsonSerialize(using = LongJsonSerializer.class)
    private Long parentId;

    @ApiModelProperty(value = "实体配置")
    @TableField("entity_id")
    @JsonDeserialize(using = LongJsonDeserializer.class)
    @JsonSerialize(using = LongJsonSerializer.class)
    private Long entityId;


    @ApiModelProperty(value = "属性类型")
    @TableField("type")
    private Integer type;


}
