package com.rh.log;


import com.didispace.swagger.butler.EnableSwaggerButler;
import com.rh.cloumn.InitMetaObjectHandler;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import renhai.util.GlobalExceptionHandler;
import renhai.util.RestTemplateUtil;
import renhai.util.SpringUtil;

/**
 * @author 57556
 */
@EnableSwaggerButler
@SpringBootApplication
@MapperScan("com.rh.log.mapper.*")
public class LogApplication {

    public static void main(String[] args) {

        SpringApplication.run(LogApplication.class, args);
    }

    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    @LoadBalanced
    public RestTemplateUtil restTemplateUtil() {
        return new RestTemplateUtil();
    }
    /**
     * 默认填充字段
     */
    @Bean
    public InitMetaObjectHandler metaObjectHandler() {
        return new InitMetaObjectHandler();
    }

    @Bean
    public SpringUtil springUtil() {
        return new SpringUtil();
    }

    @Bean
    public GlobalExceptionHandler globalExceptionHandler() {
        return new GlobalExceptionHandler();
    }

}