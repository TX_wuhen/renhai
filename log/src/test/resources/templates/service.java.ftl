package ${package.Service};

import ${package.Entity}.${entity};
import ${superServiceClassPackage};
import com.github.pagehelper.PageInfo;
import renhai.util.Page;

/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {

   PageInfo<${entity}> list(Page page);

}
</#if>
