package renhai.util.redis;

/**
 * @author 57556
 */
public class RedisUrl {

    private static final String URL = "http://RENHAI-REDIS";

    public static final String SAVE_TOKEN = URL + "/api/redis/saveToken";
    /**
     * 检查token
     */
    public static final String CHECK_TOKEN = URL + "/api/redis/checkToken";
    public static final String REDIS_CACHE_SET = URL + "/api/redis/set";
    public static final String REDIS_CACHE_GET = URL + "/api/redis/get";
    public static final String REDIS_CACHE_REMOVE = URL + "/api/redis/remove";
    public static final String REDIS_CACHE_CLEAR = URL + "/api/redis/clear";
    public static final String REDIS_CACHE_GETSIZE = URL + "/api/redis/getSize";



}
