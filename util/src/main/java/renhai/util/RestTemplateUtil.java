package renhai.util;

import com.alibaba.fastjson.JSONObject;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.http.HttpServletRequestWrapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import renhai.util.redis.RedisUrl;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 57556
 */
@Component
public class RestTemplateUtil {

    @Autowired
    private RestTemplate restTemplate;

    public <T> ResponseContext getForObject(String url, T t) {
        System.out.println("RestTemplateUtil："+url);
        System.out.println("RestTemplateUtil："+JSONObject.toJSONString(t));
        return restTemplate.getForObject(url, ResponseContext.class, t);
    }

    public <T> ResponseContext postForObject(String url, T t) {

        return restTemplate.postForObject(url, t, ResponseContext.class);
    }
    public <T> ResponseContext postForObject2(String url, T t) {
        MultiValueMap<String, Object> headers = new LinkedMultiValueMap<String, Object>();
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        HttpEntity request = new HttpEntity(t, headers);
        return restTemplate.postForObject(url,request, ResponseContext.class);
    }

    public ResponseContext checkToken() {
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();
        HttpServletRequestWrapper httpServletRequestWrapper = (HttpServletRequestWrapper) request;
        String token = httpServletRequestWrapper.getRequest().getHeader("token");
        if (StringUtils.isNotBlank(token)) {
            Map<String, String> map = new HashMap<>(1);
            map.put("key", token);
            return postForObject(RedisUrl.CHECK_TOKEN, map);
        }
        return null;
    }

    public <T> T getUser(Class<T> clazz) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        assert requestAttributes != null;
        HttpServletRequest request = requestAttributes.getRequest();
        String token = request.getHeader("token");
        if (StringUtils.isNotBlank(token)) {
            Map<String, String> map = new HashMap<>(1);
            map.put("key", token);
            ResponseContext rc = postForObject(RedisUrl.REDIS_CACHE_GET, map);
            if(rc!=null && rc.getEntity()!=null){
                String entity = rc.getEntity().toString();
                return JSONObject.parseObject(entity, clazz);
            }

        }
        return null;
    }
}
