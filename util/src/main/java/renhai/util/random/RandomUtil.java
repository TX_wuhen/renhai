package renhai.util.random;

import kohgylw.kcnamer.core.KCNamer;
import kohgylw.kcnamer.core.NameLength;

import java.io.UnsupportedEncodingException;
import java.util.Random;

/**
 * 随机生成常见的汉字
 *
 * @author xuliugen
 */
public class RandomUtil {

    private static char getRandomChar() {
        String str = "";
        int hightPos; //
        int lowPos;

        Random random = new Random();

        hightPos = (176 + Math.abs(random.nextInt(39)));
        lowPos = (161 + Math.abs(random.nextInt(93)));

        byte[] b = new byte[2];
        b[0] = (Integer.valueOf(hightPos)).byteValue();
        b[1] = (Integer.valueOf(lowPos)).byteValue();

        try {
            str = new String(b, "GBK");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            System.out.println("错误");
        }

        return str.charAt(0);
    }

    /**
     * 生成随机指定长度的中文字符串
     *
     * @param length
     * @return
     */
    public static String getCNString(Integer length) {
        StringBuilder sb = new StringBuilder(length);
        if (length > 0) {
            for (int i = 0; i < length; i++) {
                sb.append(getRandomChar());
            }
        }
        return sb.toString();
    }

    /**
     * 生成随机指定长度的英文字符串
     *
     * @param length
     * @return
     */
    public static String getEString(int length) {
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(3);
            long result = 0;
            switch (number) {
                case 0:
                    result = Math.round(Math.random() * 25 + 65);
                    sb.append((char) result);
                    break;
                case 1:
                    result = Math.round(Math.random() * 25 + 97);
                    sb.append((char) result);
                    break;
                case 2:
                    sb.append(new Random().nextInt(10));
                    break;
            }
        }
        return sb.toString();
    }

    /**
     * 获取随机姓名
     *
     * @return
     */
    public static String getCNName() {
        KCNamer kcNamer = new KCNamer();
        return kcNamer.getComRandomName();
    }

    ;

    /**
     * 获取指定长度性别的随机姓名
     *
     * @param length 姓名长度 2 3 4
     * @param sex    性别 1 男 2 女
     * @return
     */
    public static String getCNName(Integer length, Integer sex) {
        KCNamer kcNamer = new KCNamer();
        if (length > 1) {
            if (sex == 1) {
                switch (length) {
                    case 2:
                        return kcNamer.getRandomMaleName(NameLength.TWO);
                    case 4:
                        return kcNamer.getRandomMaleName(NameLength.THREE);
                    default:
                        return kcNamer.getRandomMaleName(NameLength.FOUR);
                }
            } else if (sex == 2) {
                switch (length) {
                    case 2:
                        return kcNamer.getRandomFemaleName(NameLength.TWO);
                    case 4:
                        return kcNamer.getRandomFemaleName(NameLength.THREE);
                    default:
                        return kcNamer.getRandomFemaleName(NameLength.FOUR);
                }
            } else {
                return kcNamer.getComRandomName();
            }
        }
        return "";
    }

    ;
}
