package renhai.util;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 57556
 */
@Getter
@Setter
public class ResponseContext<T> {
    /**
     * 数据
     */
    private List<T> data;
    /**
     * 状态码
     */
    private Integer code;
    /**
     * 说明信息
     */
    private String msg;
    /**
     * 状态 1正常 0错误
     */
    private Integer state = 1;

    private T entity;

    public ResponseContext(T entity, String msg, Integer state) {
        this.entity = entity;
        this.msg = msg;
        this.state = state;
    }

    public ResponseContext(T entity, Integer state) {
        this.entity = entity;
        this.state = state;
    }

    public ResponseContext(T entity, Integer state, String msg) {
        this.entity = entity;
        this.state = state;
        this.msg = msg;
    }

    public ResponseContext(String msg, Integer state, Integer code) {
        this.msg = msg;
        this.state = state;
        this.code = code;
    }

    public ResponseContext(T entity, String msg) {
        this.entity = entity;
        this.msg = msg;
    }

    public ResponseContext(T entity) {
        this.entity = entity;
    }

    public ResponseContext(List<T> data, String msg, Integer state) {
        this.data = data;
        this.msg = msg;
        this.state = state;
    }

    public ResponseContext(List<T> data, String msg) {
        this.data = data;
        this.msg = msg;
    }

    public ResponseContext(List<T> data, Integer state) {
        this.data = data;
        this.state = state;
    }

    public ResponseContext(List<T> data) {
        this.data = data;
    }

    public ResponseContext(String msg) {
        this.msg = msg;
    }

    public ResponseContext(Integer state) {
        this.state = state;
    }

    ResponseContext(Integer state, String msg) {
        this.state = state;
        this.msg = msg;
    }

    public ResponseContext() {

    }

    public List<T> getData(List<T> data) {
        if (data == null) {
            return new ArrayList<>();
        }
        return this.data;
    }


}
