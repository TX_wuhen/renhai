SET FOREIGN_KEY_CHECKS=0;
#模块
DROP TABLE IF EXISTS `ren_hai_module`;
CREATE TABLE `ren_hai_module` (
  `module_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `module_url` varchar(255) DEFAULT NULL,
  `module_grade` varchar(255) DEFAULT NULL COMMENT '模块等级(1目录，2菜单)',
  `module_authority` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `module_pid` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#角色
DROP TABLE IF EXISTS `ren_hai_role`;
CREATE TABLE `ren_hai_role` (
  `role_id` varchar(40) NOT NULL,
  `role_name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `status` int(11) DEFAULT NULL,
  `role_type` varchar(255) DEFAULT '' COMMENT '角色类型',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#角色模块中间表
DROP TABLE IF EXISTS `ren_hai_role_module`;
CREATE TABLE `ren_hai_role_module` (
  `role_module_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ren_hai_role` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ren_hai_module` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `status` int(5) DEFAULT NULL,
  PRIMARY KEY (`role_module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
#用户
DROP TABLE IF EXISTS `ren_hai_user`;
CREATE TABLE `ren_hai_user` (
  `user_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#用户角色中间表
DROP TABLE IF EXISTS `ren_hai_user_role`;
CREATE TABLE `ren_hai_user_role` (
  `user_role_id` varchar(40) NOT NULL,
  `ren_hai_user` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ren_hai_role` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#测试
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name1` varchar(50) DEFAULT NULL,
  `name2` varchar(255) DEFAULT NULL,
  `name3` varchar(255) DEFAULT NULL,
  `name4` varchar(255) DEFAULT NULL,
  `name5` varchar(255) DEFAULT NULL,
  `name6` varchar(255) DEFAULT NULL,
  `name7` varchar(255) DEFAULT NULL,
  `name8` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1351386 DEFAULT CHARSET=utf8;
#文件
DROP TABLE IF EXISTS `ren_hai_file`;
CREATE TABLE `ren_hai_file` (
                      `file_id` varchar(40) NOT NULL ,
                      `url` varchar(50) DEFAULT NULL,#文件地址
                      `md5` varchar(50) DEFAULT NULL,#md5值
                      `complete` int DEFAULT NULL,#是否上传完成
                      `chunk_size` int DEFAULT NULL,#分块大小，根据 totalSize 和这个值你就可以计算出总共的块数。注意最后一块的大小可能会比这个要大
                      `total_chunks` int DEFAULT NULL,#文件被分成的块数
                      `total_size` int DEFAULT NULL,#文件总大小
                      `status` int(11) DEFAULT NULL,
                      `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
                      `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
                      PRIMARY KEY (`file_id`),
                      KEY `id_index` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
#文件 详情
DROP TABLE IF EXISTS `ren_hai_file_details`;
CREATE TABLE `ren_hai_file_details` (
      `file_details_id` varchar(40) NOT NULL ,
      `ren_hai_file` varchar(40) NOT NULL,
      `chunk_number` int DEFAULT NULL,#当前快
      `total_chunks` int DEFAULT NULL,#文件被分成的块数
      `total_size` int DEFAULT NULL,#文件总大小
      `current_chunk_size` int DEFAULT NULL,#当前快的实际大小
      `status` int(11) DEFAULT NULL,
      `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
      `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`file_details_id`),
      KEY `id_index` (`file_details_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

