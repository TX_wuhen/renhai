import * as request from "@/axios/axios";
import store from "@/store";

let _ = require("lodash");

export function entitySaveUpdate(param: {}) {
  return request.post(
    store.state.url.user + "/api/users/renHaiEntity/saveUpdate",
    param
  );
}

export function entityList(param: {}) {
  return request.post(
    store.state.url.user + "/api/users/renHaiEntity/getList",
    param
  );
}

export function filedSaveUpdate(param: {}) {
    return request.post(
        store.state.url.user + "/api/users/renHaiField/saveUpdate",
        param
    );
}

export function filedList(param: {}) {
    return request.post(
        store.state.url.user + "/api/users/renHaiField/getList",
        param
    );
}
