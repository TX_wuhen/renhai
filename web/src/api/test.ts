import * as request from "@/axios/axios";
import store from "@/store";

export function saveUpdate(param: {}) {
  return request.post(
    store.state.url.user + "/api/users/renHaiTest/saveUpdate",
    param
  );
}
export function del(param: {}) {
  return request.del(store.state.url.user + "/api/users/renHaiTest/del", param);
}
export function details(param: {}) {
  return request.post(
    store.state.url.user + "/api/users/user/getDetails",
    param
  );
}
/**
 * 检查用户名是否重复 同步请求
 * @param param
 */
export async function checkCode(param: {}) {
  return await request.post(
    store.state.url.user + "/api/users/renHaiTest/checkCode",
    param
  );
}
