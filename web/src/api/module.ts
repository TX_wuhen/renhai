import * as request from "@/axios/axios";
import store from "@/store";

export function saveUpdate(param: {}) {
  return request.post(
    store.state.url.user + "/api/users/renHaiModule/saveUpdate",
    param
  );
}

export function del(param: {}) {
  return request.del(
    store.state.url.user + "/api/users/renHaiModule/delete",
    param
  );
}
export function get(param: {}) {
  return request.get(
    store.state.url.user + "/api/users/renHaiModule/get",
    param
  );
}
