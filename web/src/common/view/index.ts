import * as request from "@/axios/axios";

/**
 * 获取候选参数
 * @param url
 * @param param
 * @param method
 */
export function getRequest(url: string, param: {}, method: string = "post") {
  return request.requestAssign(url, param, method);
}
