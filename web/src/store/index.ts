import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const rou: any = Object;

export default new Vuex.Store({
  state: {
    token: "",
    routerLength: 0, //路由数量
    router: rou,
    url: {
      user: "/user/",
      file: "/file/",
      log: "/log/",
      util: {
        image: "/util/image.html" // 图片上传
      }
    },
    menuShow: 2 // 1 name 2 path
  },
  mutations: {},
  actions: {},
  modules: {}
});
