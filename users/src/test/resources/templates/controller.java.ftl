package ${package.Controller};


import org.springframework.web.bind.annotation.RequestMapping;

<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>
import ${package.Service}.${table.serviceName};
import ${package.Entity}.${entity};
import renhai.util.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import renhai.util.ResponseContext;
import java.io.Serializable;

/**
* <p>
    * ${table.comment!} 前端控制器
    * </p>
*
* @author ${author}
* @since ${date}
*/
<#if restControllerStyle>
    @RestController
<#else>
    @Controller
</#if>
@RequestMapping("<#if package.ModuleName??>/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
    public class ${table.controllerName} extends ${superControllerClass} {
<#else>
    public class ${table.controllerName} {
</#if>


    private final  ${table.serviceName} ${table.serviceName?uncap_first};

    @Autowired
    public ${table.controllerName}(${table.serviceName} ${table.serviceName?uncap_first}) {
        this.${table.serviceName?uncap_first} = ${table.serviceName?uncap_first};
    }

    /**
    * 查询分页数据
    */
    @ApiOperation(value = "查询分页数据")
    @PostMapping(value = "/getList")
    public ResponseContext<PageInfo<${entity}>> getList(@RequestBody Page<${entity}> page) {

        return new ResponseContext<>(${table.serviceName?uncap_first}.list(page));
    }


    /**
    * 根据id查询
    */
    @ApiOperation(value = "根据id查询数据")
    @GetMapping(value = "/getById")
    public ResponseContext<${entity}> getById(@RequestParam("id") Serializable id){
        return new ResponseContext<>(${table.serviceName?uncap_first}.getById(id));
    }

    /**
    * 新增
    */
    @ApiOperation(value = "新增/修改 数据")
    @PostMapping(value = "/saveUpdate")
    public ResponseContext<Boolean> saveUpdate(@RequestBody ${entity} ${entity?uncap_first}){
        return new ResponseContext<>(${table.serviceName?uncap_first}.saveOrUpdate(${entity?uncap_first}));
    }

    /**
    * 删除
    */
    @ApiOperation(value = "删除数据")
    @DeleteMapping(value = "/del")
    public ResponseContext<Boolean> delete(@RequestParam("id") Serializable id){
        return new ResponseContext<>(${table.serviceName?uncap_first}.removeById(id));
    }
}
        </#if>
