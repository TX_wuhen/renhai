package com.rh.users.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import renhai.util.Constant;
import renhai.util.ResponseContext;

import java.util.Map;

/** 下拉菜单
 * @author 57556
 */
@RestController
@Api("下拉框选择数据常量")
@RequestMapping("/api/users/ren/hai/base/constant")
public class BaseConstantController {

    @ApiOperation(value = "查询公共组件类型")
    @GetMapping("/getCommonType")
    private ResponseContext<Map<Integer, String>> getCommonType(){

        return new ResponseContext<>(Constant.COMMON_TYPE);
    };

    @ApiOperation(value = "获取测试表类型")
    @RequestMapping(value = "/getTestType")
    public ResponseContext<Map<Integer,String>> getTestType(){
        return new ResponseContext<>(Constant.TEST_TYPE);
    }

}
