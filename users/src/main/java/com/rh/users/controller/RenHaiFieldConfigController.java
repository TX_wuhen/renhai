package com.rh.users.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.rh.entity.users.RenHaiFieldConfig;
import com.rh.users.service.RenHaiFieldConfigService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import renhai.util.Page;
import renhai.util.ResponseContext;

import java.io.Serializable;

/**
* <p>
    *   前端控制器
    * </p>
*
* @author wangdao
* @since 2020-11-21
*/
    @RestController
@RequestMapping("/api/users/renHaiFieldConfig")
    public class RenHaiFieldConfigController {


    private final  RenHaiFieldConfigService renHaiFieldConfigService;

    @Autowired
    public RenHaiFieldConfigController(RenHaiFieldConfigService renHaiFieldConfigService) {
        this.renHaiFieldConfigService = renHaiFieldConfigService;
    }

    /**
    * 查询分页数据
    */
    @ApiOperation(value = "查询分页数据")
    @PostMapping(value = "/getList")
    public ResponseContext<PageInfo<RenHaiFieldConfig>> getList(@RequestBody Page<RenHaiFieldConfig> page) {

        return new ResponseContext<>(renHaiFieldConfigService.list(page));
    }

    @ApiOperation(value = "查询分页数据")
    @PostMapping(value = "/getTreeDate")
    public ResponseContext<RenHaiFieldConfig> getList(@RequestBody RenHaiFieldConfig config) {
        QueryWrapper<RenHaiFieldConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(config.getParentId()!=null, RenHaiFieldConfig::getParentId, config.getParentId());
        queryWrapper.lambda().eq(config.getFieldId()!=null, RenHaiFieldConfig::getFieldId, config.getFieldId());
        return new ResponseContext<>(renHaiFieldConfigService.list(queryWrapper));
    }
    /**
    * 根据id查询
    */
    @ApiOperation(value = "根据id查询数据")
    @GetMapping(value = "/getById")
    public ResponseContext<RenHaiFieldConfig> getById(@RequestParam("id") Serializable id){
        return new ResponseContext<>(renHaiFieldConfigService.getById(id));
    }

    /**
    * 新增
    */
    @ApiOperation(value = "新增/修改 数据")
    @PostMapping(value = "/saveUpdate")
    public ResponseContext<Boolean> saveUpdate(@RequestBody RenHaiFieldConfig renHaiFieldConfig){
        return new ResponseContext<>(renHaiFieldConfigService.saveOrUpdate(renHaiFieldConfig));
    }

    /**
    * 删除
    */
    @ApiOperation(value = "删除数据")
    @DeleteMapping(value = "/del")
    public ResponseContext<Boolean> delete(@RequestParam("id") Serializable id){
        return new ResponseContext<>(renHaiFieldConfigService.removeById(id));
    }
}
