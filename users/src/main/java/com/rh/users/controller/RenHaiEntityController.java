package com.rh.users.controller;


import com.github.pagehelper.PageInfo;
import com.rh.entity.users.RenHaiEntity;
import com.rh.users.service.RenHaiEntityService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import renhai.util.Page;
import renhai.util.ResponseContext;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wangdao
 * @since 2020-11-21
 */
@RestController
@RequestMapping("/api/users/renHaiEntity")
public class RenHaiEntityController {


    private final RenHaiEntityService renHaiEntityService;

    @Autowired
    public RenHaiEntityController(RenHaiEntityService renHaiEntityService) {
        this.renHaiEntityService = renHaiEntityService;
    }

    /**
     * 查询分页数据
     */
    @ApiOperation(value = "查询分页数据")
    @PostMapping(value = "/getList")
    public ResponseContext<PageInfo<RenHaiEntity>> getList(@RequestBody Page<RenHaiEntity> page) {

        return new ResponseContext<>(renHaiEntityService.list(page));
    }


    /**
     * 根据id查询
     */
    @ApiOperation(value = "根据id查询数据")
    @PostMapping(value = "/getById")
    public ResponseContext<RenHaiEntity> getById(@RequestBody RenHaiEntity entity) {
        return new ResponseContext<>(renHaiEntityService.getById(entity.getId()));
    }

    /**
     * 新增
     */
    @ApiOperation(value = "新增/修改 数据")
    @PostMapping(value = "/saveUpdate")
    public ResponseContext<Boolean> saveUpdate(@RequestBody RenHaiEntity renHaiEntity) {
        return new ResponseContext<>(renHaiEntityService.saveOrUpdate(renHaiEntity));
    }

    /**
     * 删除
     */
    @ApiOperation(value = "删除数据")
    @DeleteMapping(value = "/del")
    public ResponseContext<Boolean> delete(@RequestBody RenHaiEntity entity) {
        return new ResponseContext<>(renHaiEntityService.removeById(entity.getId()));
    }


}
