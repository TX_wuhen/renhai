package com.rh.users.controller;


import com.github.pagehelper.PageInfo;
import com.rh.entity.users.RenHaiCommonModule;
import com.rh.users.service.RenHaiCommonModuleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import renhai.util.Page;
import renhai.util.ResponseContext;

import java.io.Serializable;

/**
* <p>
    * 公共组件  前端控制器
    * </p>
*
* @author wangdao
* @since 2020-07-22
*/
@RestController
@RequestMapping("/api/users/ren/hai/common/module")
    public class RenHaiCommonModuleController {


    private final  RenHaiCommonModuleService renHaiCommonModuleService;

    @Autowired
    public RenHaiCommonModuleController(RenHaiCommonModuleService renHaiCommonModuleService) {
        this.renHaiCommonModuleService = renHaiCommonModuleService;
    }

    /**
    * 查询分页数据
    */
    @ApiOperation(value = "查询分页数据")
    @GetMapping(value = "/getList")
    public ResponseContext<PageInfo<RenHaiCommonModule>> getList(@RequestBody Page<RenHaiCommonModule> page) {

        return new ResponseContext<>(renHaiCommonModuleService.list(page));
    }


    /**
    * 根据id查询
    */
    @ApiOperation(value = "根据id查询数据")
    @GetMapping(value = "/getById")
    public ResponseContext<RenHaiCommonModule> getById(@RequestParam("id") Serializable id){
        return new ResponseContext<>(renHaiCommonModuleService.getById(id));
    }

    /**
    * 新增
    */
    @ApiOperation(value = "新增/修改 数据")
    @PostMapping(value = "/saveUpdate")
    public ResponseContext<Boolean> saveUpdate(@RequestBody RenHaiCommonModule renHaiCommonModule){
        return new ResponseContext<>(renHaiCommonModuleService.saveOrUpdate(renHaiCommonModule));
    }

    /**
    * 删除
    */
    @ApiOperation(value = "删除数据")
    @DeleteMapping(value = "/del")
    public ResponseContext<Boolean> delete(@RequestParam("id") Serializable id){
        return new ResponseContext<>(renHaiCommonModuleService.removeById(id));
    }
}
