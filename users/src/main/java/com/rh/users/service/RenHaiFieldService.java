package com.rh.users.service;

import com.rh.entity.users.RenHaiField;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import renhai.util.Page;

/**
 * <p>
 *   服务类
 * </p>
 *
 * @author wangdao
 * @since 2020-11-21
 */
public interface RenHaiFieldService extends IService<RenHaiField> {

   PageInfo<RenHaiField> list(Page<RenHaiField> page);

}
