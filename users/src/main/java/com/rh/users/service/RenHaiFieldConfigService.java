package com.rh.users.service;

import com.rh.entity.users.RenHaiFieldConfig;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import renhai.util.Page;

/**
 * <p>
 *   服务类
 * </p>
 *
 * @author wangdao
 * @since 2020-11-21
 */
public interface RenHaiFieldConfigService extends IService<RenHaiFieldConfig> {

   PageInfo<RenHaiFieldConfig> list(Page<RenHaiFieldConfig> page);

}
