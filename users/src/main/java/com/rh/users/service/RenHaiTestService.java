package com.rh.users.service;

import com.rh.entity.users.RenHaiTest;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import renhai.util.Page;

/**
 * <p>
 *   服务类
 * </p>
 *
 * @author wangdao
 * @since 2020-05-21
 */
public interface RenHaiTestService extends IService<RenHaiTest> {

   PageInfo<RenHaiTest> list(Page<RenHaiTest> page);

}
