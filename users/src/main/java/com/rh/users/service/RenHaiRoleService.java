package com.rh.users.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.rh.entity.role.RenHaiRole;
import renhai.util.Page;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
public interface RenHaiRoleService extends IService<RenHaiRole> {
    /**
     * @param role      角色信息
     * @param moduleIds 角色拥有的模块id
     * @return true
     */
    boolean save(RenHaiRole role, String[] moduleIds);

    /**
     * @param role      角色信息
     * @param moduleIds 角色拥有的模块id
     * @return true/false
     */
    boolean update(RenHaiRole role, String[] moduleIds);

    PageInfo list(Page page, RenHaiRole role);

    /**
     * 角色详情
     *
     * @param id 角色id
     * @return
     */
    RenHaiRole getDetails(String id);

    /**
     * 删除角色
     *
     * @param id
     * @return
     */
    boolean deleteById(String id);
}
