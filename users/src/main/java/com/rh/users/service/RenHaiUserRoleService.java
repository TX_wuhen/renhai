package com.rh.users.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rh.entity.user.RenHaiUserRole;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
public interface RenHaiUserRoleService extends IService<RenHaiUserRole> {

    boolean saveBatch(String[] roleIds, String userId);

    boolean delete(String userId, String roleIds);

    List<RenHaiUserRole> getList(String userId, String roleId);

}
