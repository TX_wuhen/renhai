package com.rh.users.service;

/**
 * @Author: wangdao
 * @Date: 2020/12/12 19:10
 */
public interface InitConfigServer {
    /**
     * 初始化配置
     */
    void init();

}
