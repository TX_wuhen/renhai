package com.rh.users.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ArrayUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rh.entity.user.RenHaiUserRole;
import com.rh.users.mapper.RenHaiUserRoleMapper;
import com.rh.users.service.RenHaiUserRoleService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
@Service
public class RenHaiUserRoleServiceImpl extends ServiceImpl<RenHaiUserRoleMapper, RenHaiUserRole> implements RenHaiUserRoleService {

    private final RenHaiUserRoleMapper mapper;

    public RenHaiUserRoleServiceImpl(RenHaiUserRoleMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public boolean saveBatch(String[] roleIds, String userId) {
        if (ArrayUtils.isNotEmpty(roleIds) && StringUtils.isNotBlank(userId)) {
            List<RenHaiUserRole> list = new ArrayList<>();
            Arrays.asList(roleIds).forEach(id -> list.add(new RenHaiUserRole(userId, id)));
            //批量插入
            if (!list.isEmpty()) {
                return this.saveBatch(list, 500);
            }
        }
        return false;
    }

    @Override
    public boolean delete(String userId, String roleId) {
        QueryWrapper<RenHaiUserRole> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                //角色id是否为null
                .eq(StringUtils.isNotBlank(roleId), RenHaiUserRole::getRenHaiRole, roleId)
                //用户id是否为null
                .eq(StringUtils.isNotBlank(userId), RenHaiUserRole::getRenHaiUser, userId);
        mapper.delete(wrapper);
        return true;
    }

    @Override
    public List<RenHaiUserRole> getList(String userId, String roleId) {
        QueryWrapper<RenHaiUserRole> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                //角色id是否为null
                .eq(StringUtils.isNotBlank(roleId), RenHaiUserRole::getRenHaiRole, roleId)
                //用户id是否为null
                .eq(StringUtils.isNotBlank(userId), RenHaiUserRole::getRenHaiUser, userId);
        return mapper.selectList(wrapper);
    }


}
