package com.rh.users.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rh.entity.role.RenHaiRoleModule;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
public interface RenHaiRoleModuleService extends IService<RenHaiRoleModule> {

    boolean saveBatch(String[] moduleIds, String roleId);

    boolean delete(String roleId, String moduleId);

    List<RenHaiRoleModule> getList(String roleId, String moduleId);

}
