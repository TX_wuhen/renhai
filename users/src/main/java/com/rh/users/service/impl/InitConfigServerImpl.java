package com.rh.users.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.rh.entity.users.RenHaiEntity;
import com.rh.entity.users.RenHaiField;
import com.rh.entity.users.RenHaiFieldConfig;
import com.rh.users.service.InitConfigServer;
import com.rh.users.service.RenHaiEntityService;
import com.rh.users.service.RenHaiFieldConfigService;
import com.rh.users.service.RenHaiFieldService;
import com.rh.users.util.FiledConfigUtil;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: wangdao
 * @Date: 2020/12/12 19:11
 */
@Service
public class InitConfigServerImpl  implements InitConfigServer {

    private final RenHaiEntityService entityService;
    private final RenHaiFieldService fieldService;
    private final RenHaiFieldConfigService fieldConfigService;

    public InitConfigServerImpl(RenHaiEntityService entityService, RenHaiFieldService fieldService, RenHaiFieldConfigService fieldConfigService) {
        this.entityService = entityService;
        this.fieldService = fieldService;
        this.fieldConfigService = fieldConfigService;
    }

    @Override
    public void init() {
        RenHaiEntity entity = new RenHaiEntity(){{
            setName("intiEnity");
            setTitle("初始实体");
        }};
        // 初始化实体
        if(entityService.save(entity)){
            //初始化字段
            List<RenHaiField> list = initFields(entity);
            if(list!=null && !list.isEmpty()){
                /**
                 * 初始化字段配置
                 */
                list.forEach(this::initFieldConfig);
            }
        }

    }

    List<RenHaiField> initFields(RenHaiEntity entity){
        // 初始化字段
        List<RenHaiField> list = new ArrayList<>();
        list.add(new RenHaiField(){{
            setName("id");
            setTitle("id");
            setLength(32);
            setEntityId(entity.getId());
        }});
        list.add(new RenHaiField(){{
            setName("name");
            setTitle("名称");
            setLength(32);
            setEntityId(entity.getId());
        }});
        list.add(new RenHaiField(){{
            setName("创建人");
            setTitle("createId");
            setLength(32);
            setEntityId(entity.getId());
        }});
        list.add(new RenHaiField(){{
            setName("createName");
            setTitle("创建人姓名");
            setLength(32);
            setEntityId(entity.getId());
        }});
        list.add(new RenHaiField(){{
            setName("createTime");
            setTitle("创建事件");
            setLength(32);
            setEntityId(entity.getId());
        }});
        list.add(new RenHaiField(){{
            setName("updateId");
            setTitle("更新人");
            setLength(32);
            setEntityId(entity.getId());
        }});
        list.add(new RenHaiField(){{
            setName("updateName");
            setTitle("更新人姓名");
            setLength(32);
        }});
        list.add(new RenHaiField(){{
            setName("updateTime");
            setTitle("修改时间");
            setLength(32);
            setEntityId(entity.getId());
        }});
        if(fieldService.saveBatch(list)){
            return list;
        }
        return null;
    }


    void initFieldConfig(RenHaiField field){
        fieldConfigService.save(new RenHaiFieldConfig(){{
            setName("key");
            setType(FiledConfigUtil.map.get(Boolean.class));
            setEntityId(field.getEntityId());
            setFieldId(field.getId());
        }});
        fieldConfigService.save(new RenHaiFieldConfig(){{
            setName("id");
            setType(FiledConfigUtil.map.get(Long.class));
            setEntityId(field.getEntityId());
            setFieldId(field.getId());
        }});
        fieldConfigService.save(new RenHaiFieldConfig(){{
            setName("tableShow");
            setType(FiledConfigUtil.map.get(Long.class));
            setEntityId(field.getEntityId());
            setFieldId(field.getId());
        }});
        fieldConfigService.save(new RenHaiFieldConfig(){{
            setName("name");
            setType(FiledConfigUtil.map.get(String.class));
            setEntityId(field.getEntityId());
            setFieldId(field.getId());
        }});
        fieldConfigService.save(new RenHaiFieldConfig(){{
            setName("formShow");
            setType(FiledConfigUtil.map.get(Boolean.class));
            setEntityId(field.getEntityId());
            setFieldId(field.getId());
        }});
        fieldConfigService.save(new RenHaiFieldConfig(){{
            setName("tableParam");
            setType(FiledConfigUtil.map.get(JsonNode.class));
            setEntityId(field.getEntityId());
            setFieldId(field.getId());
        }});


        RenHaiFieldConfig formParam = new RenHaiFieldConfig() {{
            setName("formParam");
            setType(FiledConfigUtil.map.get(JsonNode.class));
            setEntityId(field.getEntityId());
            setFieldId(field.getId());
        }};
        if(fieldConfigService.save(formParam)){
            fieldConfigService.save(new RenHaiFieldConfig(){{
                setParentId(formParam.getId());
                setName("jsonString");
                setType(FiledConfigUtil.map.get(Boolean.class));
                setEntityId(field.getEntityId());
                setFieldId(field.getId());
            }});
            fieldConfigService.save(new RenHaiFieldConfig(){{
                setParentId(formParam.getId());
                setName("multi");
                setType(FiledConfigUtil.map.get(Boolean.class));
                setEntityId(field.getEntityId());
                setFieldId(field.getId());
            }});
            fieldConfigService.save(new RenHaiFieldConfig(){{
                setParentId(formParam.getId());
                setName("createState");
                setType(FiledConfigUtil.map.get(Boolean.class));
                setEntityId(field.getEntityId());
                setFieldId(field.getId());
            }});
            RenHaiFieldConfig createParams = new RenHaiFieldConfig() {{
                setParentId(formParam.getId());
                setName("createParams");
                setType(FiledConfigUtil.map.get(JsonNode.class));
                setEntityId(field.getEntityId());
                setFieldId(field.getId());
            }};
            if(fieldConfigService.save(createParams)){
                fieldConfigService.save(new RenHaiFieldConfig(){{
                    setParentId(createParams.getId());
                    setName("name");
                    setType(FiledConfigUtil.map.get(String.class));
                    setEntityId(field.getEntityId());
                    setFieldId(field.getId());
                }});
                fieldConfigService.save(new RenHaiFieldConfig(){{
                    setParentId(createParams.getId());
                    setName("index");
                    setType(FiledConfigUtil.map.get(Integer.class));
                    setEntityId(field.getEntityId());
                    setFieldId(field.getId());
                }});
            }
            fieldConfigService.save(new RenHaiFieldConfig(){{
                setParentId(formParam.getId());
                setName("updateState");
                setType(FiledConfigUtil.map.get(Boolean.class));
                setEntityId(field.getEntityId());
                setFieldId(field.getId());
            }});
            RenHaiFieldConfig updateParams = new RenHaiFieldConfig() {{
                setParentId(formParam.getId());
                setName("updateParams");
                setType(FiledConfigUtil.map.get(Boolean.class));
                setEntityId(field.getEntityId());
                setFieldId(field.getId());
            }};
            if(fieldConfigService.save(updateParams)){
                fieldConfigService.save(new RenHaiFieldConfig(){{
                    setParentId(updateParams.getId());
                    setName("name");
                    setType(FiledConfigUtil.map.get(String.class));
                    setEntityId(field.getEntityId());
                    setFieldId(field.getId());
                }});
                fieldConfigService.save(new RenHaiFieldConfig(){{
                    setParentId(updateParams.getId());
                    setName("state");
                    setType(FiledConfigUtil.map.get(Boolean.class));
                    setEntityId(field.getEntityId());
                    setFieldId(field.getId());
                }});
                fieldConfigService.save(new RenHaiFieldConfig(){{
                    setParentId(updateParams.getId());
                    setName("index");
                    setType(FiledConfigUtil.map.get(Integer.class));
                    setEntityId(field.getEntityId());
                    setFieldId(field.getId());
                }});
            }
            fieldConfigService.save(new RenHaiFieldConfig(){{
                setParentId(formParam.getId());
                setName("detailsState");
                setType(FiledConfigUtil.map.get(Boolean.class));
                setEntityId(field.getEntityId());
                setFieldId(field.getId());
            }});
            RenHaiFieldConfig detailsParams = new RenHaiFieldConfig() {{
                setParentId(formParam.getId());
                setName("detailsParams");
                setType(FiledConfigUtil.map.get(JsonNode.class));
                setEntityId(field.getEntityId());
                setFieldId(field.getId());
            }};
            if(fieldConfigService.save(detailsParams)){
                fieldConfigService.save(new RenHaiFieldConfig(){{
                    setParentId(detailsParams.getId());
                    setName("name");
                    setType(FiledConfigUtil.map.get(String.class));
                    setEntityId(field.getEntityId());
                    setFieldId(field.getId());
                }});
                fieldConfigService.save(new RenHaiFieldConfig(){{
                    setParentId(detailsParams.getId());
                    setName("index");
                    setType(FiledConfigUtil.map.get(Integer.class));
                    setEntityId(field.getEntityId());
                    setFieldId(field.getId());
                }});
            }
            fieldConfigService.save(new RenHaiFieldConfig(){{
                setParentId(formParam.getId());
                setName("params");
                setType(FiledConfigUtil.map.get(JsonNode.class));
                setEntityId(field.getEntityId());
                setFieldId(field.getId());
            }});











        }




    }

}
