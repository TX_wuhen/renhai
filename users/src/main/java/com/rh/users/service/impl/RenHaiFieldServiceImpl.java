package com.rh.users.service.impl;

import com.rh.entity.users.RenHaiField;
import com.rh.users.mapper.RenHaiFieldMapper;
import com.rh.users.service.RenHaiFieldService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import renhai.util.Page;
import renhai.util.RestTemplateUtil;

/**
 * <p>
 *   服务实现类
 * </p>
 *
 * @author wangdao
 * @since 2020-11-21
 */
@Service
public class RenHaiFieldServiceImpl extends ServiceImpl<RenHaiFieldMapper, RenHaiField> implements RenHaiFieldService {

   private final RenHaiFieldMapper renHaiFieldMapper;

   private final RestTemplateUtil restTemplateUtil;

   public  RenHaiFieldServiceImpl(RenHaiFieldMapper renHaiFieldMapper, RestTemplateUtil restTemplateUtil) {
     this.renHaiFieldMapper = renHaiFieldMapper;
     this.restTemplateUtil = restTemplateUtil;
   }

   @Override
   public PageInfo<RenHaiField> list(Page<RenHaiField> page) {
     QueryWrapper<RenHaiField> queryWrapper = new QueryWrapper<>();
     RenHaiField params = page.getParams();
     queryWrapper.lambda().eq(params.getEntityId()!=null,RenHaiField::getEntityId,params.getEntityId());
     PageHelper.startPage(page);
     return new PageInfo<>(renHaiFieldMapper.selectList(queryWrapper));
   }
}
