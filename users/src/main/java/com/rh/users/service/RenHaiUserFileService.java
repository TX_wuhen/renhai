package com.rh.users.service;

import com.rh.entity.user.RenHaiUserFile;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import renhai.util.Page;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangdao
 * @since 2020-04-29
 */
public interface RenHaiUserFileService extends IService<RenHaiUserFile> {

   PageInfo<RenHaiUserFile> list(Page page);

}
