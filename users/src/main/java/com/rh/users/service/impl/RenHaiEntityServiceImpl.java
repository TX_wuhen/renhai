package com.rh.users.service.impl;

import com.rh.entity.users.RenHaiEntity;
import com.rh.users.mapper.RenHaiEntityMapper;
import com.rh.users.service.RenHaiEntityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import renhai.util.Page;
import renhai.util.RestTemplateUtil;

import java.util.List;

/**
 * <p>
 *   服务实现类
 * </p>
 *
 * @author wangdao
 * @since 2020-11-21
 */
@Service
public class RenHaiEntityServiceImpl extends ServiceImpl<RenHaiEntityMapper, RenHaiEntity> implements RenHaiEntityService {

   private final RenHaiEntityMapper renHaiEntityMapper;

   private final RestTemplateUtil restTemplateUtil;

   public  RenHaiEntityServiceImpl(RenHaiEntityMapper renHaiEntityMapper, RestTemplateUtil restTemplateUtil) {
     this.renHaiEntityMapper = renHaiEntityMapper;
     this.restTemplateUtil = restTemplateUtil;
   }

   @Override
   public PageInfo<RenHaiEntity> list(Page<RenHaiEntity> page) {
     QueryWrapper<RenHaiEntity> queryWrapper = new QueryWrapper<>();
     PageHelper.startPage(page);
       List<RenHaiEntity> renHaiEntities = renHaiEntityMapper.selectList(queryWrapper);
       return new PageInfo<>(renHaiEntityMapper.selectList(queryWrapper));
   }
}
