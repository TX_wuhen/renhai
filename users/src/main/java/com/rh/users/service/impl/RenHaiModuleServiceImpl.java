package com.rh.users.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.rh.entity.module.RenHaiModule;
import com.rh.entity.user.RenHaiUser;
import com.rh.result.module.RenHaiModuleResult;
import com.rh.users.mapper.RenHaiModuleMapper;
import com.rh.users.service.RenHaiModuleService;
import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import renhai.util.Page;
import renhai.util.RestTemplateUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
@Service
public class RenHaiModuleServiceImpl extends ServiceImpl<RenHaiModuleMapper, RenHaiModule> implements RenHaiModuleService {

    private final RenHaiModuleMapper mapper;

    private final RestTemplateUtil restTemplateUtil;

    public RenHaiModuleServiceImpl(RenHaiModuleMapper mapper, RestTemplateUtil restTemplateUtil) {
        this.mapper = mapper;
        this.restTemplateUtil = restTemplateUtil;
    }

    /**
     * 修改模块
     *
     * @param module
     */
    @Override
    public int update(RenHaiModule module) {
        return mapper.update(module, new UpdateWrapper<RenHaiModule>()
                .lambda()
                // 是否更新          字段                          值
                .set(true, RenHaiModule::getModuleName, module.getModuleName())
                .set(true, RenHaiModule::getModuleAuthority, module.getModuleAuthority())
                .set(true, RenHaiModule::getModuleGrade, module.getModuleGrade())
                .set(RenHaiModule::getModuleUrl, module.getModuleUrl())
                .eq(RenHaiModule::getModuleId, module.getModuleId()));
    }

    /**
     * 获取模块菜单树
     *
     * @param module 模块
     * @return 模块树
     */
    @Override
    public List<RenHaiModule> getListTree(RenHaiModule module) {
        List<RenHaiModule> list = this.list();
        return getTree(list);
    }

    /**
     * 获取模块树
     *
     * @param list 模块列表
     * @return
     */
    private List<RenHaiModule> getTree(List<RenHaiModule> list) {
        List<RenHaiModule> moduleList = null;
        //判断是否有数据
        if (list != null && !list.isEmpty()) {
            //查找全部的一级模块 以及模块没有PID
            moduleList = list.stream()
                    .filter(m -> m.getModulePid() == null ||  "".equals(m.getModulePid()))
                    .collect(Collectors.toList());
        }
        //设置子模块
        if (moduleList != null && !moduleList.isEmpty()) {
            moduleList.forEach(rhModule -> {
                rhModule.setChildren(getChild(rhModule.getModuleId(), list));
            });
        }
        return moduleList;
    }

    ;

    /**
     * 递归查找子菜单
     *
     * @param id      当前菜单id
     * @param allList 要查找的列表
     * @return 子数据
     */
    private List<RenHaiModule> getChild(String id, List<RenHaiModule> allList) {
        // 子菜单
        List<RenHaiModule> childList = new ArrayList<>();
        if (allList != null && !allList.isEmpty()) {
            //全部数据
            allList.forEach(module -> {
                // 遍历所有节点，将父菜单id与传过来的id比较
                if (module.getModulePid() != null && StringUtils.isNotBlank(module.getModulePid())) {
                    if (module.getModulePid().equals(id)) {
                        childList.add(module);
                    }
                }
            });
        }
        if (!childList.isEmpty()) {
            // 把子菜单的子菜单再循环一遍
            childList.forEach(module -> {
                if ((module.getModulePid() != null && StringUtils.isNotBlank(module.getModulePid()))) {
                    assert module.getModulePid() != null;
                    if (module.getModulePid().equals(id)) {
                        module.setChildren(getChild(module.getModuleId(), allList));
                    }
                }
            });
        }
        // 递归退出条件
        if (childList.isEmpty()) {
            return null;
        }
        return childList;
    }

    @Override
    public List<RenHaiModule> getMenu() {
        RenHaiUser user = restTemplateUtil.getUser(RenHaiUser.class);
        List<RenHaiModule> list = mapper.getMenu(user.getUserId());
        return getTree(list);
    }

    @Override
    public PageInfo<RenHaiModule> list(Page page, RenHaiModule module) {
        QueryWrapper<RenHaiModule> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(!"".equals(module.getModuleId()) && module.getModuleId() != null, RenHaiModule::getModulePid, module.getModuleId())
                .and(module.getModuleId() == null || "".equals(module.getModuleId()),
                        wrapper -> wrapper.isNull(RenHaiModule::getModulePid)
                                .or()
                                .eq(RenHaiModule::getModulePid, ""))
                .orderByAsc(RenHaiModule::getCreateTime);
        PageHelper.startPage(page);
        return new PageInfo<>(mapper.selectList(queryWrapper));
    }

    @Override
    public RenHaiModuleResult getById(String id) {
        RenHaiModuleResult renHaiModuleResult = new RenHaiModuleResult();
        BeanUtils.copyProperties( mapper.selectById(id),renHaiModuleResult);
        if(StringUtils.isNotEmpty(renHaiModuleResult.getModulePid())){
            renHaiModuleResult.setPModule(mapper.selectById(renHaiModuleResult.getModulePid()));
        }else{
            renHaiModuleResult.setPModule(new RenHaiModule(){{setModuleName("无");}});
        }
        return renHaiModuleResult;
    }

}
