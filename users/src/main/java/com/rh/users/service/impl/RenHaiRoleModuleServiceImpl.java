package com.rh.users.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rh.entity.role.RenHaiRoleModule;
import com.rh.users.mapper.RenHaiRoleModuleMapper;
import com.rh.users.service.RenHaiRoleModuleService;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
@Service
public class RenHaiRoleModuleServiceImpl extends ServiceImpl<RenHaiRoleModuleMapper, RenHaiRoleModule> implements RenHaiRoleModuleService {

    private final RenHaiRoleModuleMapper mapper;

    public RenHaiRoleModuleServiceImpl(RenHaiRoleModuleMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public boolean saveBatch(String[] moduleIds, String roleId) {
        if (ArrayUtils.isNotEmpty(moduleIds) && StringUtils.isNotBlank(roleId)) {
            List<RenHaiRoleModule> list = new ArrayList<>();
            Arrays.asList(moduleIds).forEach(id -> list.add(new RenHaiRoleModule(roleId, id)));
            //批量插入
            if (!list.isEmpty()) {
                return this.saveBatch(list, 500);
            }
        }
        return false;
    }

    @Override
    public boolean delete(String roleId, String moduleId) {
        QueryWrapper<RenHaiRoleModule> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                //角色id是否为null
                .eq(StringUtils.isNotBlank(roleId), RenHaiRoleModule::getRenHaiRole, roleId)
                //模块地是否为null
                .eq(StringUtils.isNotBlank(moduleId), RenHaiRoleModule::getRenHaiModule, moduleId);
        return mapper.delete(wrapper) > 0;
    }

    @Override
    public List<RenHaiRoleModule> getList(String roleId, String moduleId) {
        QueryWrapper<RenHaiRoleModule> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                //角色id是否为null
                .eq(StringUtils.isNotBlank(roleId), RenHaiRoleModule::getRenHaiRole, roleId)
                //模块地是否为null
                .eq(StringUtils.isNotBlank(moduleId), RenHaiRoleModule::getRenHaiModule, moduleId);
        return mapper.selectList(wrapper);
    }

    ;
}
