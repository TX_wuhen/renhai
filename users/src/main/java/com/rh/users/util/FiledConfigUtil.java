package com.rh.users.util;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: wangdao
 * @Date: 2020/12/12 19:36
 */
public class FiledConfigUtil{

    public static Map<Class,Integer> map = new HashMap<Class,Integer>(){{
        put(Boolean.class, 1);
        put(Integer.class,2);
        put(Long.class,3);
        put(Double.class,4);
        put(Date.class,5);
        put(String.class,6);
        put(JsonNode.class,7);
    }};




}
