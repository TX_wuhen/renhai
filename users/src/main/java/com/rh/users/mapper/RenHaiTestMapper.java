package com.rh.users.mapper;

import com.rh.entity.users.RenHaiTest;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
* <p>
    *   Mapper 接口
    * </p>
*
* @author wangdao
* @since 2020-05-21
*/
@Mapper
@Repository
public interface RenHaiTestMapper extends BaseMapper<RenHaiTest> {

}
