package com.rh.users.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rh.entity.module.RenHaiModule;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
@Mapper
@Repository
public interface RenHaiModuleMapper extends BaseMapper<RenHaiModule> {
    /**
     * @param userId 查询用户菜单
     * @return 查询用户菜单
     */
    List<RenHaiModule> getMenu(@Param("userId") String userId);

}
