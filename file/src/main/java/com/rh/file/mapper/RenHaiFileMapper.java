package com.rh.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rh.entity.file.RenHaiFile;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RenHaiFileMapper extends BaseMapper<RenHaiFile> {

}
