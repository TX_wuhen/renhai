package com.rh.file.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rh.entity.file.RenHaiFile;
import com.rh.entity.file.RenHaiFileDetails;
import com.rh.file.controller.FileParam;
import com.rh.file.mapper.RenHaiFileDetailsMapper;
import com.rh.file.service.RenHaiFileDetailsService;
import org.springframework.stereotype.Service;

@Service
public class RenHaiFileDetailsServiceImpl extends ServiceImpl<RenHaiFileDetailsMapper, RenHaiFileDetails> implements RenHaiFileDetailsService {


    private final RenHaiFileDetailsMapper renHaiFileDetailsMapper;

    public RenHaiFileDetailsServiceImpl(RenHaiFileDetailsMapper renHaiFileDetailsMapper) {
        this.renHaiFileDetailsMapper = renHaiFileDetailsMapper;
    }

    @Override
    public boolean deleteById(String fileId) {
        renHaiFileDetailsMapper.deleteById(fileId);
        return true;
    };

    @Override
    public RenHaiFileDetails insert(FileParam param) {
        RenHaiFileDetails file = new RenHaiFileDetails();
        //分成的块数
        file.setTotalChunks(param.getTotalChunks());
        //文件总大小
        file.setTotalSize(param.getTotalSize());
        //第几快
        file.setChunkNumber(param.getChunkNumber());
        /**
         * 实际大小
         */
        file.setCurrentChunkSize(param.getCurrentChunkSize());

        RenHaiFile file1 = new RenHaiFile();
        file1.setFileId(param.getTaskId());
        file.setRenHaiFile(file1);
        this.save(file);
        return file;
    }
    //检查该分片是否上传





}
