package com.rh.file.service;

import com.rh.file.controller.FileParam;

import java.io.IOException;


public interface FileService {
    boolean chunkUploadByMappedByteBuffer(FileParam param) throws IOException;
}
