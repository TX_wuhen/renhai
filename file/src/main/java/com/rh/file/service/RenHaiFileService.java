package com.rh.file.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rh.entity.file.RenHaiFile;
import com.rh.file.controller.FileParam;

public interface RenHaiFileService extends IService<RenHaiFile> {
    /**
     * 检查文件是否上传
     *
     * @param param
     * @return
     */
    RenHaiFile checkUploading(FileParam param);

    RenHaiFile insert(FileParam param);

    RenHaiFile getByMD5(FileParam param);

    void updateComplete(String findId,String url);
}
