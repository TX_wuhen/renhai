package com.rh.file.controller;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件传输对象
 */
@ApiModel("大文件分片入参实体")
@Getter
@Setter
public class FileParam {
    @ApiModelProperty("当前文件次序，第一块是1")
    private Long chunkNumber;
    @ApiModelProperty("文件被分成的块数")
    private Integer totalChunks;
    @ApiModelProperty("分块大小，根据 totalSize 和这个值你就可以计算出总共的块数。注意最后一块的大小可能会比这个要大")
    private Integer chunkSize;
    @ApiModelProperty("当前块的大小，实际大小。")
    private Integer currentChunkSize;
    @ApiModelProperty("文件总大小")
    private Long totalSize;
    @ApiModelProperty("每个文件的唯一标示")//md5
    private String identifier;
    @ApiModelProperty("文件名")
    private String filename;
    @ApiModelProperty("文件夹上传的时候文件的相对路径属性")
    private String relativePath;
    @ApiModelProperty("文件传输任务ID")//uuid
    private String taskId;
    @ApiModelProperty("路径")
    private String path;
    @ApiModelProperty("分块文件传输对象")
    private MultipartFile file;

    public String getTaskId() {
        if (identifier != null) {
            String[] temp = identifier.split(",");
            if (temp.length == 2) {
                return temp[0];
            }
        }
        return null;
    }

    public String getIdentifier() {//md5
        if (identifier != null) {
            String[] temp = identifier.split(",");
            if (temp.length == 2) {
                return temp[1];
            }
        }
        return identifier;
    }
}