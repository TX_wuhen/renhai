package com.rh.file.util;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "upload")
@Getter
@Setter
@Api("上传文件参数配置")
public class FileUpload {

    @ApiModelProperty("上传路径")
    private String path;

    @ApiModelProperty("访问路径")
    private String visitPath;

    @ApiModelProperty("是否sftp")
    private boolean sftp;

    @ApiModelProperty("是否ftp")
    private boolean ftp;

    @ApiModelProperty("地址")
    private String url;

    @ApiModelProperty("ftp/sftp 用户名")
    private String user;

    @ApiModelProperty("ftp/sftp 密码")
    private String pwd;


}
